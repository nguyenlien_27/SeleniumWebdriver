package firefox;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class ff_bt8_VerifyTitleAndImage {
	private WebDriver driver;

	@Before
	public void before() {
		System.setProperty("webdriver.gecko.driver", "geckodriver.exe");
		driver = new FirefoxDriver();
	}

	@Test
	public void verifyTitleAndImage() throws InterruptedException{
	driver.get("http://digitest.vn/mantis/login_page.php");
	Thread.sleep(3000);
	
	//Verify Logo
	String path = driver.findElement(By.xpath("html/body/div[1]/a/img")).getAttribute("src");
	Assert.assertEquals("http://digitest.vn/mantis/images/mantis_logo.png",path);
	System.out.println("********** VERIFY LOGO URL **********");
	System.out.println("Logo URL is correct: " + path);
	if(path.equals("http://digitest.vn/mantis/images/mantis_logo.png")){
		System.out.println("Good! It�s nice logo\n");
	}else{
		System.out.println("Please check the logo again�!!!");
	}

	//Verify Title
	String expectedTtile = "MantisBT";
	Assert.assertEquals("MantisBT",expectedTtile);	
	System.out.println("********** VERIFY TITLE **********");
	if(driver.getTitle().equals(expectedTtile)){
		System.out.println("Title is correct:  " + expectedTtile);
		System.out.println("The current URL: " + driver.getCurrentUrl());
	}else{
		System.out.println("Title is not correct");
		System.out.println(driver.getCurrentUrl());
	}
	
	//Verify Title of img
	//Login																								
	driver.get("http://digitest.vn/mantis/login_page.php");																								
	driver.findElement(By.name("username")).sendKeys("test");																								
	driver.findElement(By.name("password")).sendKeys("testing");																								
	driver.findElement(By.xpath("html/body/div[3]/form/table/tbody/tr[6]/td/input")).click();																								
	Thread.sleep(10000);
	
	//Verify img title
	String path1 = driver.findElement(By.xpath("html/body/table[3]/tbody/tr/td[2]/div/a/img")).getAttribute("alt");
	Assert.assertEquals("Powered by Mantis Bugtracker",path1);
	System.out.println("********** VERIFY LOGO ITTLE **********");
	System.out.println("Logo title is " + path1);
	
	}
	

	@After
	public void after() {
//		driver.quit();
	}
}
