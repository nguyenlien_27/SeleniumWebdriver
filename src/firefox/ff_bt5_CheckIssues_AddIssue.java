package firefox;																									
																									
import org.junit.After;
import org.junit.Before;
import org.junit.Test;																									
import org.openqa.selenium.By;																									
import org.openqa.selenium.WebDriver;																									
import org.openqa.selenium.firefox.FirefoxDriver;																									
																									
public class ff_bt5_CheckIssues_AddIssue {																									
	private WebDriver driver;																								
																									
	@Before																								
	// run Firefox																								
	public void before() {																								
		System.getProperty("webdriver.gecko.driver", "geckodriver.exe");																							
		driver = new FirefoxDriver();																							
	}																								
																									
	@Test																								
	public void addIssueAndDel() throws InterruptedException{																								
	// Login																								
	driver.get("http://digitest.vn/mantis/login_page.php");																								
	driver.findElement(By.name("username")).sendKeys("test");																								
	driver.findElement(By.name("password")).sendKeys("testing");																								
	driver.findElement(By.xpath("html/body/div[3]/form/table/tbody/tr[6]/td/input")).click();																								
	Thread.sleep(3000);																								
	// Check issue is exist or not																								
	//view issue list																								
	driver.findElement(By.xpath("html/body/table[2]/tbody/tr/td[1]/a[2]")).click();																								
	Thread.sleep(5000);																								
	// Delete if the issue is exsit																								
	driver.findElement(By.name("search")).sendKeys("0948251919");																								
	driver.findElement(By.name("filter")).click();																								
	Thread.sleep(3000);																								
	String VerifySearch = driver.findElement(By.xpath(".//*[@id='buglist']/tbody/tr[1]/td/span[1]")).getText();																								
	if(!VerifySearch.equals("Viewing Issues (0 - 0 / 0)")){		
		//del the issue
		driver.findElement(By.name("all_bugs")).click();																							
		driver.findElement(By.name("action")).sendKeys("Delete");																						
		driver.findElement(By.xpath(".//*[@id='buglist']/tbody/tr[5]/td/span[1]/input[2]")).click();																							
		Thread.sleep(3000);																							
		driver.findElement(By.xpath("html/body/div[2]/form/table/tbody/tr[2]/td/input")).click();																							
		Thread.sleep(3000);
		
		//create issue
		driver.findElement(By.linkText("Report Issue")).click();																							
		Thread.sleep(3000);																							
		driver.findElement(By.name("category_id")).sendKeys("[All Projects] Layout - CSS");																							
		driver.findElement(By.name("reproducibility")).sendKeys("always");																							
		driver.findElement(By.name("severity")).sendKeys("crash");																							
		driver.findElement(By.name("priority")).sendKeys("urgent");																							
		driver.findElement(By.id("platform")).sendKeys("entry platform");																							
		driver.findElement(By.id("os")).sendKeys("entry os");																							
		driver.findElement(By.id("os_build")).sendKeys("entry os build");																							
		driver.findElement(By.name("handler_id")).sendKeys("entry assignee");																							
		driver.findElement(By.name("summary")).sendKeys("0948251919");																							
		driver.findElement(By.name("description")).sendKeys("entry description");																							
		driver.findElement(By.name("steps_to_reproduce")).sendKeys("entry how to reproduce");																							
		driver.findElement(By.name("additional_info")).sendKeys("entry additional information");																							
		driver.findElement(By.xpath("html/body/div[3]/form/table/tbody/tr[14]/td[2]/label[2]")).click();																							
		driver.findElement(By.xpath("html/body/div[3]/form/table/tbody/tr[15]/td[2]/label")).click();																							
		driver.findElement(By.xpath("html/body/div[3]/form/table/tbody/tr[16]/td[2]/input")).click();																							
		Thread.sleep(3000);	
	}																								
	else{//create issue																								
		driver.findElement(By.linkText("Report Issue")).click();																							
		Thread.sleep(3000);																							
		driver.findElement(By.name("category_id")).sendKeys("[All Projects] Layout - CSS");																							
		driver.findElement(By.name("reproducibility")).sendKeys("always");																							
		driver.findElement(By.name("severity")).sendKeys("crash");																							
		driver.findElement(By.name("priority")).sendKeys("urgent");																							
		driver.findElement(By.id("platform")).sendKeys("entry platform");																							
		driver.findElement(By.id("os")).sendKeys("entry os");																							
		driver.findElement(By.id("os_build")).sendKeys("entry os build");																							
		driver.findElement(By.name("handler_id")).sendKeys("entry assignee");																							
		driver.findElement(By.name("summary")).sendKeys("0948251919");																							
		driver.findElement(By.name("description")).sendKeys("entry description");																							
		driver.findElement(By.name("steps_to_reproduce")).sendKeys("entry how to reproduce");																							
		driver.findElement(By.name("additional_info")).sendKeys("entry additional information");																							
		driver.findElement(By.xpath("html/body/div[3]/form/table/tbody/tr[14]/td[2]/label[2]")).click();																							
		driver.findElement(By.xpath("html/body/div[3]/form/table/tbody/tr[15]/td[2]/label")).click();																							
		driver.findElement(By.xpath("html/body/div[3]/form/table/tbody/tr[16]/td[2]/input")).click();																							
		Thread.sleep(3000);																							
	}																								
	//View issues list																								
	driver.findElement(By.linkText("View Issues")).click();																								
	Thread.sleep(3000);																								

	
	//Find the issue that has been added (take care: don't automatically del the inputted text to search					
	driver.findElement(By.xpath(".//*[@id='filters_form_open']/table/tbody/tr[11]/td[1]/input[1]")).clear();	
	driver.findElement(By.xpath(".//*[@id='filters_form_open']/table/tbody/tr[11]/td[1]/input[1]")).sendKeys("0948251919");																								
	driver.findElement(By.name("filter")).click();																								
	Thread.sleep(5000);																								

/*	
	String VerifySummary = driver.findElement(By.xpath(".//*[@id='buglist']/tbody/tr[4]/td[11]")).getText();																								
	Assert.assertEquals("0948251919",VerifySummary);																								
	//Del the issues that has been added																								
	driver.findElement(By.name("all_bugs")).click();																								
	driver.findElement(By.name("action")).sendKeys("Delete");																								
	Thread.sleep(3000);																								
	driver.findElement(By.xpath(".//*[@id='buglist']/tbody/tr[5]/td/span[1]/input[2]")).click();																								
	Thread.sleep(3000);																								
	driver.findElement(By.xpath("html/body/div[2]/form/table/tbody/tr[2]/td/input")).click();																								
*/																									
	//Log out																								
	driver.findElement(By.linkText("Logout")).click();																								
	Thread.sleep(3000);																								
	}																								
	@After																								
	// Close the firefox																								
	public void after() throws Exception {																								
		driver.quit();																							
	}																								
}																																								