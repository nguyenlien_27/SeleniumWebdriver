package firefox;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.After;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;


public class ff_checkLogin {

	private WebDriver driver; //khai bao bien driver

	@Before //dieu kien truoc test (bat firefox)
	public void before() {
		System.setProperty("webdriver.gecko.driver", "geckodriver.exe");
		driver = new FirefoxDriver();
	}

	@Test
	public void login() throws InterruptedException {
		driver.get("http://digitest.vn/alpha/press/wp-login.php"); //mo url
		driver.findElement(By.xpath(".//*[@id='user_login']")).sendKeys("Author"); //by.xpath mo bang xpath, co the thay bang by.id con sendky la nhap gia tri
		driver.findElement(By.xpath(".//*[@id='user_pass']")).sendKeys("Author2017");
		driver.findElement(By.xpath(".//*[@id='wp-submit']")).click(); //lenh click 
		Thread.sleep(8000);//doi 8 giay
		String VerifyText = driver.findElement(By.xpath(".//*[@id='wpbody-content']/div[4]/h1")).getText(); //tim den xpath de verify text 
		Assert.assertEquals("Dashboard", VerifyText);//dau tien la gia tri mong muon, tiep theo la gia tri thuc te tren web
	}

	@After
	public void tearDown() throws Exception {
		driver.quit(); //sau khi test xong thi thoat trinh duyet
	}

}
