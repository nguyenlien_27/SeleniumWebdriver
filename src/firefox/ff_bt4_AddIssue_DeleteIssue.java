package firefox;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class ff_bt4_AddIssue_DeleteIssue {
	private WebDriver driver;
	@Before
	public void before(){
		System.setProperty("webdriver.gecko.driver","geckodriver.exe");
		driver = new FirefoxDriver();
	}
	@Test
	public void reportIssues() throws InterruptedException{
		//LOGIN
		driver.get("http://digitest.vn/mantis/login_page.php");
		driver.findElement(By.name("username")).sendKeys("test");
		driver.findElement(By.name("password")).sendKeys("testing");
		driver.findElement(By.xpath("html/body/div[3]/form/table/tbody/tr[6]/td/input")).click();
		Thread.sleep(3000);
		String VerifyLogin = driver.findElement(By.xpath("html/body/table[1]/tbody/tr/td[1]")).getText();
		Assert.assertEquals("Logged in as: test (sasa - developer)",VerifyLogin);		
		
		//REPORT ISSUE
		//Open report issue page and input data
		driver.findElement(By.linkText("Report Issue")).click();
		Thread.sleep(8000);
		driver.findElement(By.name("category_id")).sendKeys("[All Projects] Layout - CSS");
		driver.findElement(By.name("reproducibility")).sendKeys("always");
		driver.findElement(By.name("severity")).sendKeys("crash");
		driver.findElement(By.name("priority")).sendKeys("urgent");
		driver.findElement(By.id("platform")).sendKeys("entry platform");
		driver.findElement(By.id("os")).sendKeys("entry os");
		driver.findElement(By.id("os_build")).sendKeys("entry os build");
		driver.findElement(By.name("handler_id")).sendKeys("entry assignee");
		driver.findElement(By.name("summary")).sendKeys("0948251919");
		driver.findElement(By.name("description")).sendKeys("entry description");
		driver.findElement(By.name("steps_to_reproduce")).sendKeys("entry how to reproduce");
		driver.findElement(By.name("additional_info")).sendKeys("entry additional information");
		driver.findElement(By.xpath("html/body/div[3]/form/table/tbody/tr[14]/td[2]/label[2]")).click();
		driver.findElement(By.xpath("html/body/div[3]/form/table/tbody/tr[15]/td[2]/label")).click();
		driver.findElement(By.xpath("html/body/div[3]/form/table/tbody/tr[16]/td[2]/input")).click();
		Thread.sleep(3000);
		
		//View issues list
		driver.findElement(By.linkText("View Issues")).click();
		Thread.sleep(3000);		

		//Find the issue that has been added
		driver.findElement(By.xpath(".//*[@id='filters_form_open']/table/tbody/tr[11]/td[1]/input[1]")).sendKeys("0948251919");
		driver.findElement(By.name("filter")).click();
		Thread.sleep(5000);
		String VerifySummary = driver.findElement(By.xpath(".//*[@id='buglist']/tbody/tr[4]/td[11]")).getText();
		Assert.assertEquals("0948251919",VerifySummary);
		
		//Del the issues that has been added
		driver.findElement(By.name("bug_arr[]")).click();
		driver.findElement(By.name("action")).sendKeys("Delete");
		Thread.sleep(3000);
		driver.findElement(By.xpath(".//*[@id='buglist']/tbody/tr[5]/td/span[1]/input[2]")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("html/body/div[2]/form/table/tbody/tr[2]/td/input")).click();
		
		//Log out
		driver.findElement(By.linkText("Logout")).click();
		Thread.sleep(3000);
	}		
		
	@After
	public void after() throws Exception {
		driver.quit();
	}
}
