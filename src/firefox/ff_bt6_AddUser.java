package firefox;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class ff_bt6_AddUser {
	private WebDriver driver;

	@Before
	public void before() {
		System.getProperty("webdriver.gecko.driver", "geckodriver.exe");
		driver = new FirefoxDriver();
	}

	@Test
	public void addUser() throws InterruptedException {
		// Login
		driver.get("http://localhost/pentest1/wp-login.php");
		driver.findElement(By.id("user_login")).sendKeys("admin");
		driver.findElement(By.id("user_pass")).sendKeys("hello");
		driver.findElement(By.id("wp-submit")).click();
		Thread.sleep(5000);
		String VerifyLogin = driver.findElement(By.xpath(".//*[@id='wpbody-content']/div[5]/h2")).getText();
		Assert.assertEquals("Dashboard", VerifyLogin);
		/*
		 * //go to add user page by hover property WebElement element =
		 * driver.findElement(By.xpath(".//*[@id='menu-users']/a/div[3]"));
		 * Actions action = new Actions(driver);
		 * action.moveToElement(element).perform(); Thread.sleep(3000);
		 * WebElement subElement = driver.findElement(By.linkText("Log Out"));
		 * action.moveToElement(subElement); action.click(); action.perform();
		 */
		// load add all user page
		driver.findElement(By.xpath(".//*[@id='menu-users']/a/div[3]")).click();
		Thread.sleep(3000);

		// load the add new user page
//		driver.findElement(By.xpath(".//*[@id='wpbody-content']/div[5]/h2/a")).click();
//		Thread.sleep(3000);

		// check user is esxits or not
		driver.findElement(By.id("user-search-input")).sendKeys("user01");
		driver.findElement(By.id("search-submit")).click();
		Thread.sleep(3000);
		//because the expath sometimes changes => we will verify text on the page
		if(driver.getPageSource().contains("No matching users were found.")){
			//create user
			driver.findElement(By.xpath(".//*[@id='wpbody-content']/div[5]/h2/a")).click();
			Thread.sleep(3000);
			driver.findElement(By.id("user_login")).sendKeys("user01");
			driver.findElement(By.id("email")).sendKeys("nguyenlien.7@gmail.com");
			driver.findElement(By.id("pass1")).sendKeys("123456");
			driver.findElement(By.id("pass2")).sendKeys("123456");
			driver.findElement(By.id("createusersub")).click();
			Thread.sleep(5000);
		} else {
			//del the user
			driver.findElement(By.xpath(".//*[@id='cb-select-all-1']")).click();
			driver.findElement(By.id("bulk-action-selector-top")).sendKeys("Delete");
			Thread.sleep(3000);
			driver.findElement(By.xpath(".//*[@id='doaction']")).click();
			Thread.sleep(5000);
			driver.findElement(By.xpath(".//*[@id='delete_option0']")).click();
			driver.findElement(By.xpath(".//*[@id='submit']")).click();
			Thread.sleep(5000);
			
			//go to add new user screen then input info for new user
			driver.findElement(By.xpath(".//*[@id='wpbody-content']/div[5]/h2/a")).click();
			Thread.sleep(3000);
			driver.findElement(By.id("user_login")).sendKeys("user01");
			driver.findElement(By.id("email")).sendKeys("nguyenlien.7@gmail.com");
			driver.findElement(By.id("pass1")).sendKeys("123456");
			driver.findElement(By.id("pass2")).sendKeys("123456");
			driver.findElement(By.id("createusersub")).click();
			Thread.sleep(5000);
		
		}

	}
	// @After
}