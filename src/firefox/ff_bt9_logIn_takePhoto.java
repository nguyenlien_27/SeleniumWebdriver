package firefox;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;


//Screenshot
import java.io.File;
import java.io.IOException;
import java.util.Random;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.TakesScreenshot;

public class ff_bt9_logIn_takePhoto {
	private WebDriver driver;
	private int randomInt;
	
	@Before
	public void before() {
		System.setProperty("webdriver.gecko.dirver", "geckodriver.exe");
		driver = new FirefoxDriver();
	}

	@Test
	public void login() throws InterruptedException, IOException {
		//login
		driver.get("http://localhost/pentest1/wp-login.php");
		driver.findElement(By.id("user_login")).sendKeys("admin");
		driver.findElement(By.id("user_pass")).sendKeys("hello");
		driver.findElement(By.id("wp-submit")).click();
		Thread.sleep(8000);
//		String VerifyText = driver.findElement(By.xpath(".//*[@id='wpbody-content']/div[5]/h2")).getText();
//		Assert.assertEquals("Dashboard", VerifyText);
		
		//take photo
		Random rd = new Random();
		for (int idx = 1000; idx <= 100000; ++idx) {
		randomInt = rd.nextInt(100000);		
		}
		
		
		
		
		File src= ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(src, new File("images/"+ randomInt + ".png"));
		
		
	}

	@After
	public void after() {
		driver.quit();
	}
}