package firefox;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class ff_bt1_Login {
	private WebDriver driver;
	@Before
	public void before(){
		System.setProperty("webdriver.gecko.dirver","geckodriver.exe");
		driver = new FirefoxDriver();
	}
	@Test
	public void login() throws InterruptedException{
		driver.get("http://localhost/pentest1/wp-login.php");
		driver.findElement(By.id("user_login")).sendKeys("admin");
		driver.findElement(By.id("user_pass")).sendKeys("hello");
		driver.findElement(By.id("wp-submit")).click();
		Thread.sleep(8000);
		String VerifyText = driver.findElement(By.xpath(".//*[@id='wpbody-content']/div[5]/h2")).getText();
		Assert.assertEquals("Dashboard", VerifyText);
	}
	@After
	public void after() throws Exception {
		driver.quit();
	}
}
