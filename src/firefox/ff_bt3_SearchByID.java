package firefox;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class ff_bt3_SearchByID {
	private WebDriver driver;
	@Before
	//chay firefox
	public void before(){
		System.setProperty("webdriver.gecko.driver","geckodriver.exe");
		driver = new FirefoxDriver();
	}
	@Test
	public void login() throws InterruptedException{
		//open the URL
		driver.get("http://digitest.vn/mantis/login_page.php");
		//Enter the user name
		driver.findElement(By.name("username")).sendKeys("test");
		//Enter the password
		driver.findElement(By.name("password")).sendKeys("testing");
		//Click on Login button
		driver.findElement(By.xpath("html/body/div[3]/form/table/tbody/tr[6]/td/input")).click();
		//Wait for 3 seconds
		Thread.sleep(3000);
		//Get text by xpath
		String VerifyLogin = driver.findElement(By.xpath("html/body/table[1]/tbody/tr/td[1]")).getText();
		//Check login successful
		Assert.assertEquals("Logged in as: test (sasa - developer)",VerifyLogin);
		//View issues list
		driver.findElement(By.linkText("View Issues")).click();
		//Wait for 3 seconds
		Thread.sleep(3000);
		
		//search by ID
		driver.findElement(By.xpath(".//*[@id='filters_form_open']/table/tbody/tr[11]/td[1]/input[1]")).sendKeys("0001212");
		driver.findElement(By.name("filter")).click();
		Thread.sleep(3000);
		String VerifyId = driver.findElement(By.xpath(".//*[@id='buglist']/tbody/tr[4]/td[4]/a")).getText();
		Assert.assertEquals("0001212",VerifyId);
		
		//sign out
		Thread.sleep(3000);
		driver.findElement(By.xpath("html/body/table[2]/tbody/tr/td[1]/a[7]")).click();
		Thread.sleep(3000);
		String VerifyLogout = driver.findElement(By.xpath("html/body/div[3]/form/table/tbody/tr[1]/td[1]")).getText();
		Assert.assertEquals("Login",VerifyLogout);
	}
	@After
	public void after() throws Exception {
	driver.quit();
	}
}