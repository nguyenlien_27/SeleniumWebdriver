package firefox;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class ff_bt2_Login {
	private WebDriver driver;
	@Before
	public void before(){
		System.setProperty("webdriver.gecko.driver", "geckodriver.exe");
		driver = new FirefoxDriver();
	}
	@Test
	public void check() throws InterruptedException {
		driver.get("http://digitest.vn/mantis/login_page.php");
		driver.findElement(By.name("username")).sendKeys("test");
		driver.findElement(By.name("password")).sendKeys("testing");
		driver.findElement(By.xpath("html/body/div[3]/form/table/tbody/tr[6]/td/input")).click();
		Thread.sleep(8000);
		String VerifyText = driver.findElement(By.xpath("html/body/table[1]/tbody/tr/td[1]")).getText();
		Assert.assertEquals("Logged in as: test (sasa - developer)", VerifyText);
		driver.findElement(By.xpath("html/body/table[2]/tbody/tr/td[1]/a[7]")).click();
	}
	
	@After
	public void after() throws Exception {
		driver.quit();
	}
}
