package internetExplorer;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

public class IE_bt1_Login2 {
	private WebDriver driver;
	@Before
	public void before(){
		System.setProperty("webdriver.ie.driver", "IEDriverServer.exe");
		 
		DesiredCapabilities caps = DesiredCapabilities.internetExplorer();
		        caps.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,true);
		caps.setCapability("ignoreZoomSetting", true);
		caps.setCapability("nativeEvents",false);        
		driver = new InternetExplorerDriver(caps);    
	}
	@Test
	public void login() throws InterruptedException{
		driver.get("http://localhost/pentest1/wp-login.php");
		driver.findElement(By.id("user_login")).sendKeys("admin");
		driver.findElement(By.id("user_pass")).sendKeys("hello");
		driver.findElement(By.id("wp-submit")).click();
		Thread.sleep(8000);
		String VerifyText = driver.findElement(By.xpath(".//*[@id='wpbody-content']/div[5]/h2")).getText();
		Assert.assertEquals("Dashboard", VerifyText);
	}
	@After
	public void after() throws Exception {
		driver.close();
	}
}
